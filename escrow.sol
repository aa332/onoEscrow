pragma solidity ^0.4.19;

contract onoEscrow {

	struct Trade{
		bytes32 tradeId;
   		address buyer;   
    	address seller;
    	address arbitrator;
    	uint256 value;
    	uint256 fee;
    	uint256 balance;
    	bool fiatSent;
    	bool contractFunded;
    	bool dispute; 
	}
    // Explore modifiers  
	mapping (bytes32 => Trade) trades;

	bytes32[] public tradeIds;

	function innitTrade(address _buyer, address _seller, uint256 _value) public returns(bytes32) { 
        require(msg.sender != _buyer && msg.sender != _seller,
                "only arbitrator can start a trade");

        
		bytes32 _tradeId = keccak256(_seller, _buyer, _value, block.number, now);
		
		Trade storage trade = trades[_tradeId];

		trade.buyer = _buyer;
		trade.seller = _seller;
		trade.value = _value * 1000000000000000000; //convert to ether
		trade.fee = 75;
		trade.balance = 0;
		trade.arbitrator = msg.sender;
		
        tradeIds.push(_tradeId) -1;
        
        return _tradeId;
	}
	
    function fundEscrow(bytes32 _tradeId) public payable {
        require(msg.sender == trades[_tradeId].seller && msg.value == trades[_tradeId].value,
                "Seller not authorized or value does not match the deal");
        trades[_tradeId].contractFunded = true;
    }
    
    function confirmFiatTransaction(bytes32 _tradeId) public {
        require(msg.sender == trades[_tradeId].buyer,
                "Buyer not authorized.");
        trades[_tradeId].fiatSent = true;
    }

    function releaseEscrow(bytes32 _tradeId) public payable {
        require(trades[_tradeId].dispute != true,
                "There is a dispute, please wait for an arbitrators decision");
        require(msg.sender == trades[_tradeId].seller,
                "Seller not authorized.");
        require(trades[_tradeId].fiatSent == true,
                "Buyer did not confirm fiat transaction."); //i'm not sure do we need this or not
        
        address _buyer = trades[_tradeId].buyer;
        _buyer.transfer(trades[_tradeId].value);
    }
    
    function isDispute(bytes32 _tradeId) public {
        require(msg.sender == trades[_tradeId].seller || msg.sender == trades[_tradeId].buyer,
                "only buyer OR seller can start a dispute");
        require(trades[_tradeId].contractFunded == true,
                "contract is not funded");
        trades[_tradeId].dispute = true;
    }

    function releaseToBuyer(bytes32 _tradeId) public {
        require(msg.sender == trades[_tradeId].arbitrator,
                "Arbitrator not authorized.");
                require(trades[_tradeId].dispute == true,
                "There is no dispute taking place");
        
        address _buyer = trades[_tradeId].buyer;
        _buyer.transfer(trades[_tradeId].value);
    }

    function releaseToSeller(bytes32 _tradeId) public {
        require(msg.sender == trades[_tradeId].arbitrator,
                "Arbitrator not authorized.");
        require(trades[_tradeId].dispute == true,
                "There is no dispute taking place");
        
        address _seller = trades[_tradeId].seller;
        _seller.transfer(trades[_tradeId].value);
    }
}
